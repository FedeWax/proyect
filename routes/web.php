<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>['guest']],function()
{
	Route::get('/','Auth\LoginController@showLoginForm');
	Route::post('/','Auth\LoginController@login')->name('login'); // Te acordas que hablamos sobre ruta de get y ruta de post? si, fijate bien ESTA ruta la de "login" es solo de post, aca tenes 3 formas de solucionarlo:
	/*
	1: cambiar la funcion que te deslogea para redireccionarte a / en vez de /login
	2: Hacer lo mismo que hace la ruta / en /login
	3: Redireccionar /login a la raiz
	has tu eleccion. ;)
	*/
});

Route::group(['middleware'=>['auth']],function()
{
	Route::post('logout','Auth\LoginController@logout')->name('logout');

	Route::get('/main', function () {
    return view('contenido/contenido');
	})->name('main');

	Route::group(['middleware'=>['Almacenero']],function()
	{
			Route::get('/categorias', 'CategoriaController@index');
			Route::get('/categorias/comprobar','CategoriaController@comprobar');
			Route::post('/categorias/registrar','CategoriaController@store');
			Route::put('/categorias/actualizar','CategoriaController@update');
			Route::put('/categorias/desactivar','CategoriaController@desactivar');
			Route::put('/categorias/activar','CategoriaController@activar');
			Route::get('/categorias/selectCategoria', 'CategoriaController@selectCategoria');

			Route::get('/articulos', 'articuloController@index');
			Route::get('/articulos/comprobar','articuloController@comprobar');		
			Route::post('/articulos/registrar','articuloController@store');
			Route::put('/articulos/actualizar','articuloController@update');
			Route::put('/articulos/desactivar','articuloController@desactivar');
			Route::put('/articulos/activar','articuloController@activar');
			Route::get('/articulos/buscarArticulo','articuloController@buscarArticulo');
			Route::get('/articulos/listarArticulo','articuloController@listarArticulo');


			Route::get('/pro', 'proveedorController@index');
			Route::post('/pro/registrar', 'proveedorController@store');
			Route::put('/pro/actualizar', 'proveedorController@update');
			
			Route::get('/ingresos', 'IngresoController@index');
			Route::post('/ingresos/registrar', 'IngresoController@store');
			Route::put('/ingresos/desactivar', 'IngresoController@desactivar');
			Route::get('/proveedor/selectProveedor', 'proveedorController@selectProveedor');
	});

	Route::group(['middleware'=>['Vendedor']],function()
	{
		Route::get('/usuarios', 'usuarioController@index');
		Route::post('/usuarios/registrar', 'usuarioController@store');
		Route::put('/usuarios/actualizar', 'usuarioController@update');

	});

	  Route::group(['middleware' => ['Administrador']], function () 
	  {
		Route::get('/usuarios', 'usuarioController@index');
		Route::post('/usuarios/registrar', 'usuarioController@store');
		Route::put('/usuarios/actualizar', 'usuarioController@update');

		Route::get('/categorias', 'CategoriaController@index');
		Route::get('/categorias/comprobar','CategoriaController@comprobar');
		Route::post('/categorias/registrar','CategoriaController@store');
		Route::put('/categorias/actualizar','CategoriaController@update');
		Route::put('/categorias/desactivar','CategoriaController@desactivar');
		Route::put('/categorias/activar','CategoriaController@activar');
		Route::get('/categorias/selectCategoria', 'CategoriaController@selectCategoria');

		Route::get('/articulos', 'articuloController@index');
		Route::get('/articulos/comprobar','articuloController@comprobar');
		Route::post('/articulos/registrar','articuloController@store');
		Route::put('/articulos/actualizar','articuloController@update');
		Route::put('/articulos/desactivar','articuloController@desactivar');
		Route::put('/articulos/activar','articuloController@activar');
		Route::get('/articulos/buscarArticulo','articuloController@buscarArticulo');

		Route::get('/pro', 'proveedorController@index');
		Route::post('/pro/registrar', 'proveedorController@store');
		Route::put('/pro/actualizar', 'proveedorController@update');
		Route::get('/proveedor/selectProveedor', 'proveedorController@selectProveedor');

		Route::get('/roles', 'rolController@index');
		Route::get('/roles/selectRol', 'rolController@selectRol');

		Route::get('/users', 'userController@index');
		Route::get('/users/comprobar','userController@comprobar');
		Route::post('/users/registrar','userController@store');
		Route::put('/users/actualizar','userController@update');
		Route::put('/users/desactivar','userController@desactivar');
		Route::put('/users/activar','userController@activar');

		Route::get('/ingresos', 'IngresoController@index');
		Route::post('/ingresos/registrar', 'IngresoController@store');
		Route::put('/ingresos/desactivar', 'IngresoController@desactivar');
	});
	//enrutamiento de funciones al controlador de cateforias
	
});

//Route::get('/home', 'HomeController@index')->name('home');
