<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function comprobar(Request $request)
    {
        $nombre = $request->nombre;
        if(!$nombre =="")
        {
            $categoria = Categoria::comprobarModel($nombre);
           
            if(!empty($categoria))
              return '1';
            
            if(empty($categoria))
              return '0';
        }
    }
    public function index(Request $request)
    {     
        if (!$request->ajax()) return redirect('/');

        $noPagina = $request->noPagina;
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar == '')
            $categorias = Categoria::getCategorias($buscar,$criterio,$noPagina);
        else
            $categorias = Categoria::getCategorias($buscar,$criterio,$noPagina);

        return [
            'pagination' => [
                'total'        => $categorias->total(),
                'current_page' => $categorias->currentPage(),
                'per_page'     => $categorias->perPage(),
                'last_page'    => $categorias->lastPage(),
                'from'         => $categorias->firstItem(),
                'to'           => $categorias->lastItem(),
            ],
            'categorias' => $categorias
        ];
    }
    public function selectCategoria(Request $request)
    {
         if (!$request->ajax()) return redirect('/');

         $categorias = Categoria::where('condicion','=','1')
         ->select('id','nombre')->orderBy('nombre','asc')->get();

         return ['categorias'=> $categorias];
    }
    //ese parametro requst viene de la funcion axios de la vista trae los parametros  para hacer el insert al la condicion del es borrado le damos de valor por defecto 1 para que siempre este regisrado como activo
    public function store(Request $request)
    {
         if (!$request->ajax()) return redirect('/'); 

        $objeto = new Categoria();
        $objeto->nombre = $request->nombre;
        $objeto->descripcion = $request->descripcion;
        $objeto->condicion = '1';
        $objeto->save();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/'); 

        $categoria = Categoria::findOrFail($request->id);
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->condicion = '1';
        $categoria->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/'); 

        $categoria = Categoria::findOrFail($request->id);
        $categoria->condicion = '0';
        $categoria->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $categoria = Categoria::findOrFail($request->id);
        $categoria->condicion = '1';
        $categoria->save();
    }

}
