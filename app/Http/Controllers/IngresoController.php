<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Ingreso;
use App\DetalleIngreso;
class IngresoController extends Controller
{
    public function index(Request $request)
    {
        //if (!$request->ajax()) return redirect('/');
        
        $noPagina = $request->noPagina;
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
         if ($buscar==''){
            $ingresos = Ingreso::join('usuarios','ingresos.idproveedor','=','usuarios.id')
            ->join('users','ingresos.idusuario','=','users.id')
            ->select('ingresos.id', 'ingresos.tipo_comprobante','ingresos.serie_comprobante',
            'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total'
            ,'ingresos.estado','usuarios.nombres','users.usuario')
            ->orderBy('ingresos.id','desc')->paginate($noPagina);

        }
        else{
            $ingresos = Ingreso::join('usuarios','ingresos.idproveedor','=','usuarios.id')
            ->join('users','ingresos.idusuario','=','users.id')
            ->select('ingresos.id', 'ingresos.tipo_comprobante','ingresos.serie_comprobante',
            'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total'
            ,'ingresos.estado','usuarios.nombres','users.usuario')
            ->where('ingresos.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('ingresos.id', 'desc')->paginate($noPagina);
        }
         
 
        return [
            'pagination' => [
                'total'        => $ingresos->total(),
                'current_page' => $ingresos->currentPage(),
                'per_page'     => $ingresos->perPage(),
                'last_page'    => $ingresos->lastPage(),
                'from'         => $ingresos->firstItem(),
                'to'           => $ingresos->lastItem(),
            ],
            'ingresos' => $ingresos
        ];
    }
 
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        try{

            //aqui empieza el codigo para el carrito 
            //primero se hace el insert a ingresos luego a detalles ya que tengamos todos los detalles.

            DB::beginTransaction();
 
            $mytime= Carbon::now('America/Hermosillo');
 
            $ingreso = new Ingreso();
            $ingreso->idproveedor = $request->idproveedor;
            //asignamos el id del usuario que esta logeado a la llave foranea para la venta.
            $ingreso->idusuario = \Auth::user()->id;
            $ingreso->tipo_comprobante = $request->tipo_comprobante;
            $ingreso->serie_comprobante = $request->serie_comprobante;
            $ingreso->num_comprobante = $request->num_comprobante;
            $ingreso->fecha_hora = $mytime->toDateString();
            $ingreso->impuesto = $request->impuesto;
            $ingreso->total = $request->total;
            $ingreso->estado = 'Registrado';
            $ingreso->save();
 
            $detalles = $request->data;//Array donde tenemos todos los detalles del ingreso
            //Recorro todos los elementos
            //para poder hacer el insert de todos los detalles del ingreso
            foreach($detalles as $ep=>$det)
            {
                $detalle = new DetalleIngreso();
                $detalle->idingreso = $ingreso->id;
                $detalle->idarticulo = $det['idarticulo'];
                $detalle->cantidad = $det['cantidad'];
                $detalle->precio = $det['precio'];          
                $detalle->save();
            }          
 
            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        }
    }
 
    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $ingreso = Ingreso::findOrFail($request->id);
        $ingreso->estado = 'Anulado';
        $ingreso->save();
    }
}
