<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Usuario;
use App\Proveedor;
class proveedorController extends Controller
{
     public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $noPagina = $request->noPagina; 

        if ($buscar=='')
            $usuarios = Proveedor::getProveedores($buscar,$criterio,$noPagina);
        else 
            $usuarios = Proveedor::getProveedores($buscar,$criterio,$noPagina);
    
        return [
            'pagination' => [
                'total'        => $usuarios->total(),
                'current_page' => $usuarios->currentPage(),
                'per_page'     => $usuarios->perPage(),
                'last_page'    => $usuarios->lastPage(),
                'from'         => $usuarios->firstItem(),
                'to'           => $usuarios->lastItem(),
            ],
            'usuarios' => $usuarios
        ];
    }
    public function selectProveedor(Request $request){
        //if (!$request->ajax()) return redirect('/');
 
        $filtro = $request->filtro;
        $proveedores = Proveedor::join('usuarios','proveedores.id','=','usuarios.id')
        ->where('usuarios.nombres', 'like', '%'. $filtro . '%')
        ->orWhere('usuarios.num_documento', 'like', '%'. $filtro . '%')
        ->select('usuarios.id','usuarios.nombres','usuarios.num_documento')
        ->orderBy('usuarios.nombres', 'asc')->get();
 
        return ['proveedores' => $proveedores];
    }
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
         
        try{
            DB::beginTransaction();
            $usuario = new Usuario();
            $usuario->nombres = $request->nombres;
            $usuario->apellidos = $request->apellidos;
            $usuario->tipo_documento = $request->tipo_documento;
            $usuario->num_documento = $request->num_documento;
            $usuario->direccion = $request->direccion;
            $usuario->telefono = $request->telefono;
            $usuario->email = $request->email;
            $usuario->save();
 
            $proveedor = new Proveedor();
            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->id = $usuario->id;
            $proveedor->save();
 
            DB::commit();
 
        } catch (Exception $e){
            DB::rollBack();
        }
 
         
         
    }
 
     public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
         
        try
        {
            DB::beginTransaction();
 
            //Buscar primero el proveedor a modificar
            $proveedor = Proveedor::findOrFail($request->id);
 
            $persona = Usuario::findOrFail($proveedor->id);
 
            $persona->nombres = $request->nombres;
            $persona->apellidos = $request->apellidos;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->save();
 
             
            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->save();
 
            DB::commit();
 
        } 
        catch (Exception $e)
        {
            DB::rollBack();
        }
 
    }
}
