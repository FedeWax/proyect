<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
class usuarioController extends Controller
{
     public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $noPagina = $request->noPagina;
        
        if ($buscar=='')
            $usuarios = Usuario::getUsuarios($buscar,$criterio,$noPagina);
        else
            $usuarios = Usuario::getUsuarios($buscar,$criterio,$noPagina);
         
        return [
            'pagination' => [
                'total'        => $usuarios->total(),
                'current_page' => $usuarios->currentPage(),
                'per_page'     => $usuarios->perPage(),
                'last_page'    => $usuarios->lastPage(),
                'from'         => $usuarios->firstItem(),
                'to'           => $usuarios->lastItem(),
            ],
            'usuarios' => $usuarios
        ];
    }
 
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $usuario = new Usuario();
        $usuario->nombres = $request->nombres;
        $usuario->apellidos = $request->apelidos;
        $usuario->tipo_documento = $request->tipo_documento;
        $usuario->num_documento = $request->num_documento;
        $usuario->direccion = $request->direccion;
        $usuario->telefono = $request->telefono;
        $usuario->email = $request->email;
 
        $usuario->save();
    }
 
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $usuario = Usuario::findOrFail($request->id);
        $usuario->nombres = $request->nombres;
        $usuario->apellidos = $request->apellidos;
        $usuario->tipo_documento = $request->tipo_documento;
        $usuario->num_documento = $request->num_documento;
        $usuario->direccion = $request->direccion;
        $usuario->telefono = $request->telefono;
        $usuario->email = $request->email;
        $usuario->save();
    }
}
