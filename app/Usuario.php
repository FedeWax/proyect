<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{   
    protected $fillable = ['cuenta','nombres','apellidos','tipo_documento','num_documento','direccion','telefono','email'];

	public function proveedor()
	{
		return $this->hasOne('App\Proveedor');
	}

	public function user()
	{
		return $this->hasOne('App\User');
	}

	public static function getUsuarios($buscar = '', $criterio='', $noPagina = 5)
    {
    	if (!$buscar == "")
    	{
			return self::where($criterio, 'like','%'.$buscar.'%')->orderBy('id','desc')->paginate($noPagina);
    	
    	}
		else
		return self::orderBy('id', 'desc')->paginate($noPagina);
    
    }

}
