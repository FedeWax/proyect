<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';
    protected $fillable = ['nombre','descripcion','condicion'];
    public $timestamps = false;

    public function users()
	{
		return $this->hasMany('App\User');
    }
    
    public static function getRoles($buscar = '', $criterio='')
    {
    	if (!$buscar == "")
    	{
    		return self::orderBy('id', 'desc')->paginate(3);
    	}
        else    
    	return self::where($criterio, 'like','%'.$buscar.'%')->orderBy('id','desc')->paginate(3);
    }
}
