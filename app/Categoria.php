<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
	//esto no es necesario si se llama la tabla igual que el modelo pero sin s
    protected $table = 'categorias';

    protected $fillable = ['nombre','descripcion','condicion'];

    //relacion un articulo tiene un categoria
	public function articulos()
	{
		return $this->hasMany('App\Articulo');
	}

	public static function getCategorias($buscar = '', $criterio='', $noPagina = 5)
    {
    	if (!$buscar == "")
    	{
			return self::where($criterio, 'like','%'.$buscar.'%')->orderBy('id','desc')->paginate($noPagina);
    	}
		else
		return self::orderBy('id', 'desc')->paginate($noPagina);;
    	
	}
	
	public static function comprobarModel($nombre)
    {
        return self::select('nombre')->where('nombre', $nombre)->first();
    }
}

