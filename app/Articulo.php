<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $filelable = [
        'id_categoria','codigo','nombre','precio_venta','stock','descripcion','condicion',
        'categorias.id','categorias.nombre as nombre_categoria',
        'articulos.id','articulos.id_categoria','articulos.codigo','articulos.nombre',
        'articulos.precio_venta','articulos.stock','articulos.descripcion','articulos.condicion'
    	
    ];

    //relacion una categoria puede tener varios articulos
    public function categoria()
    {
		return $this->belongsTo('App\Categoria'); 
    }

    public static function getArticulos($buscar = '', $criterio='', $noPagina = 5)
    {
    	if (!$buscar == "")
    	{
    		return self::join('categorias','articulos.id_categoria','=','categorias.id')
            ->select('articulos.id','articulos.id_categoria','articulos.codigo','articulos.nombre','categorias.nombre as nombre_categoria','articulos.precio_venta','articulos.stock','articulos.descripcion','articulos.condicion')
            ->where('articulos.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('articulos.id', 'desc')->paginate($noPagina);
    	}
        else    
    	return self::join('categorias','articulos.id_categoria','=','categorias.id')
        ->select('articulos.id','articulos.id_categoria','articulos.codigo','articulos.nombre','categorias.nombre as nombre_categoria','articulos.precio_venta','articulos.stock','articulos.descripcion','articulos.condicion')
        ->orderBy('articulos.id', 'desc')->paginate($noPagina);
    }

    public static function comprobarModel($nombre)
    {
        return self::select('nombre')->where('nombre', $nombre)->first();
    }
}
