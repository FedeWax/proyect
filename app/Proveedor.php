<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedores';

    protected $filelable= [
    	'id',
    	'contacto',
    	'telefono_contacto',
    	'usuarios.id',
    	'usuarios.nombres',
    	'usuarios.apellidos',
    	'usuarios.tipo_documento',
        'usuarios.num_documento',
        'usuarios.direccion',
        'usuarios.telefono',
        'usuarios.email',
        'proveedores.contacto',
        'proveedores.telefono_contacto'
    ];

    public $timestamps = false;

    public function usuario()
    {
    	return $this->belongsTo('App\usuario'); 
    }

    /**
    * @param array $conditional
    */
    public static function getProveedores($buscar = '', $criterio='', $noPagina = 5)
    {
    	if (!$buscar == "")
    	{
    		return self::join('usuarios','proveedores.id','=','usuarios.id')
    			->where("usuarios.$criterio", 'LIKE', "%$buscar%")
    			->orderBy('usuarios.id', 'desc')
    			->paginate($noPagina);
    	}
        else    
    	return self::join('usuarios','proveedores.id','=','usuarios.id')->orderBy('usuarios.id', 'desc')->paginate($noPagina);
    }
}
