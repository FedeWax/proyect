<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
 
  
    protected $fillable = [
        'id', 'usuario', 'password','condicion','idrol','usuarios.id','usuarios.nombres','usuarios.apellidos','usuarios.tipo_documento',
        'usuarios.num_documento','usuarios.direccion','usuarios.telefono',
        'usuarios.email','users.usuario','users.password',
        'users.condicion','users.idrol','roles.nombre as rol'
    ];
     
    public $timestamps = false;
 
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
 
    public function rol(){
        return $this->belongsTo('App\Rol');
    }
 
    public function usuario(){
        return $this->belongsTo('App\Usuario');
    }

    public static function getUsers($buscar = '', $criterio='', $noPagina = 5)
    {
    	if (!$buscar == "")
    	{
            return self::join('usuarios','users.id','=','usuarios.id')
            ->join('roles','users.idrol','=','roles.id')
            ->select('usuarios.id','usuarios.nombres','usuarios.apellidos','usuarios.tipo_documento',
            'usuarios.num_documento','usuarios.direccion','usuarios.telefono',
            'usuarios.email','users.usuario','users.password',
            'users.condicion','users.idrol','roles.nombre as rol')            
            ->where('usuarios.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('usuarios.id', 'desc')->paginate($noPagina);
    	}
        else {  
        return self::join('usuarios','users.id','=','usuarios.id')
        ->join('roles','users.idrol','=','roles.id')
        ->select('usuarios.id','usuarios.nombres','usuarios.apellidos','usuarios.tipo_documento',
        'usuarios.num_documento','usuarios.direccion','usuarios.telefono',
        'usuarios.email','users.usuario','users.password',
        'users.condicion','users.idrol','roles.nombre as rol')
        ->orderBy('usuarios.id', 'desc')->paginate($noPagina);
        }
    }

    public static function comprobarModel($nombre)
    {
        return self::select('usuario')->where('usuario', $nombre)->first();
    }
 
}
