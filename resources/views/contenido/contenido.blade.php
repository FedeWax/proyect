@extends('principal')
@section('contenido')
 
@if(Auth::check())
        @if (Auth::user()->idrol == 1)
        <template v-if="menu==0">
            <h1>Escritorio</h1>
        </template>
 
        <template v-if="menu==1">
            <categorias></categorias>
        </template>
 
        <template v-if="menu==2">
            <articulos></articulos>
        </template>
 
        <template v-if="menu==3">
            <ingresos></ingresos>
        </template>
 
        <template v-if="menu==4">
            <proveedores></proveedores>
        </template>
 
        <template v-if="menu==5">
            <h1>Ventas</h1>
        </template>
 
        <template v-if="menu==6">
            <usuarios></usuarios>
        </template>
 
        <template v-if="menu==7">
            <users></users>
        </template>
 
        <template v-if="menu==8">
            <roles></roles>
        </template>
 
        <template v-if="menu==9">
          <ingresos></ingresos>
        </template>
 
        <template v-if="menu==10">
            <h1>Reporte de ventas</h1>
        </template>
 
        <template v-if="menu==11">
            <h1>Ayuda</h1>
        </template>
 
        <template v-if="menu==12">
            <h1>Acerca de</h1>
        </template>
        @elseif (Auth::user()->idrol == 2)
        <template v-if="menu==5">
            <h1>Ventas</h1>
        </template>
 
        <template v-if="menu==6">
            <usuarios></usuarios>
        </template>
        <template v-if="menu==10">
            <h1>Reporte de ventas</h1>
        </template>
 
        <template v-if="menu==11">
            <h1>Ayuda</h1>
        </template>
 
        <template v-if="menu==12">
            <h1>Acerca de</h1>
        </template>
        @elseif (Auth::user()->idrol == 3)
        <template v-if="menu==1">
            <categorias></categorias>
        </template>
 
        <template v-if="menu==2">
            <articulos></articulos>
        </template>
 
        <template v-if="menu==3">
            <ingresos></ingresos>
        </template>
 
        <template v-if="menu==4">
            <proveedores></proveedores>
        </template>
        <template v-if="menu==9">
            <p>Detalle de ingresos</p>
        </template>
        <template v-if="menu==11">
            <h1>Ayuda</h1>
        </template>
 
        <template v-if="menu==12">
            <h1>Acerca de</h1>
        </template>
        @else
 
        @endif
 
@endif
    
     
@endsection